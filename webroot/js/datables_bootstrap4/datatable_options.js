/**
// NOTE Datepicker Options

*/
 options_datepicker = {
         language: 'es-ES'
        ,format: 'yyyy-mm-dd'
        ,autoHide: true
 };


/**
// NOTE Datatable Options
*/

options_datatable = {

        // initComplete: function () {
        //     var api = this.api();
        //     api.$('td').click( function () {
        //         api.search( this.innerHTML ).draw();
        //     } );
        // },
        // deferRender:    true,
        // scrollY:        500,
        // scrollCollapse: true,
        // scroller:       true,
        keys:           true,
        // scrollY:        '50vh',
        // scrollCollapse: true,
        // scrollX : true,
        dom: 'Bfrtip',
        language: {
            // decimal: ".",
            // thousands: ",",
            // "lengthMenu": "Display _MENU_ records per page",
            "search":         "Buscar:",
            "paginate": {
                "first":      "Primera",
                "last":       "Ultima",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "zeroRecords": "No hay registros",
            "info": "Pagina _PAGE_ de _PAGES_",
            "infoEmpty": "Sin registros disponibles",
            "infoFiltered": "(filtrados _MAX_ registros totales)",
            buttons: {
                pageLength: {
                    _: "Filtra %d lineas",
                    '-1': "Muestra Todo"
                }
            }
        },
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 lineas', '25 lineas', '50 lineas', 'Mostrar Todo' ]
        ],
        buttons: [
             { extend: 'pageLength', text: '<i class="fa fa-filter fa-2x" aria-hidden="true"></i>' }
            ,{ extend: 'copy', text: '<i class="fa fa-clipboard fa-2x" aria-hidden="true"></i>' }
            ,{ extend: 'csv', text: '<i class="fas fa-file-csv fa-2x"></i>' }
            ,{
                  extend: 'excel'
                , text: '<i class="fas fa-file-excel fa-2x"></i>'
                // , extension: '.xlsx'
                , autoFilter: true
                , messageTop:'Detalle'
                // , header:false
                , filename:"ExportData"
                , title:"File"
              }
            // ,{
            // 			 extend: 'pdf', text: '<i class="fa fa-file-pdf-o"></i>'
            // 			, messageTop:'Detalle'
            // 			// , header:false
            // 			, filename:'ExportData'
            // 			, title:"<?php print($export)?>"
            //  }
            ,{
                  // extend: 'pdfHtml5',
              extend: 'pdf', text: '<i class="fas fa-file-pdf fa-2x"></i>'
                    , messageTop:'Detalle'
              // 			// , header:false
                    , filename:"export_file"
                    , title:""
                    ,customize: function ( doc ) {
                                                    doc.content.splice( 0, 0, {
                                                        margin: [ 0, 0, 0, 12 ],
                                                        alignment: 'left',
                                                        image: imgeHeader
                                              } );
                                          }
             }
            ,{ extend: 'print', text: '<i class="fa fa-print fa-2x"></i>' }
        ]
};

// // merge obj
// var o1 = { a: 1, b: 1, c: 1 };
// var o2 = { b: 2, c: 2 };
// var o3 = { c: 3 };
// var obj = Object.assign({}, o1, o2, o3);
// console.log(obj); // { a: 1, b: 2, c: 3 }
// function myfunc() {
//    return {"name": "bob", "number": 1};
// }
//
// var myobj = myfunc();
// console.log(myobj.name, myobj.number); // logs "bob 1"

calculate_row = function (rowset,rendimiento){
  return {
          footerCallback: function ( row, data, start, end, display ) {
              var api = this.api(), data;
              // Remove the formatting to get integer data for summation
              var intVal = function ( i ) {
                // console.log(i);
                  return typeof i === 'string' ?
                      i.replace(/[\$,]/g, '')*1 :
                      typeof i === 'number' ?
                          i : 0;
              };

              var row_column = rowset;
              denominator = numerator = denominatox = numeratox = 0 ;
              // console.log(foo === undefined); // true
              if (rendimiento !== undefined) {
                set2ndParam = true;
                row_rend = rendimiento;
              }


              for (var x = 0; x < row_column.length; x++) {
                // console.log(row_column[x]);
                var ktotal = api
                      .column( row_column[x] )
                      .data()
                      .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                      }, 0 );

                  // Total over this page
                var kpageTotal = api
                      .column( row_column[x] , { page: 'current'} )
                      .data()
                      .reduce( function (a, b) {
                          return intVal(a) + intVal(b);
                      }, 0 );

                      // console.log('set2ndParam = '+set2ndParam);
                  if (rendimiento !== undefined) {
                      if ( row_column[x] == row_rend[0] ) {
                        denominator = kpageTotal;
                        denominatox = ktotal;
                      }
                      if ( row_column[x] == row_rend[1] ) {
                        // console.log(x + ' => numerator');
                        // console.log('numerator');
                        numerator = kpageTotal;
                        numeratox = ktotal;
                      }

                      // console.log(denominator/numerator); //175597
                      if (row_column[x] == row_rend[2]) {
                          kpageTotal = denominator / numerator ;
                          ktotal = denominatox / numeratox ;
                      }

                  }
                    // console.log(ktotal);
                  // Update footer
                $( api.column( row_column[x] ).footer() ).html(
                 ''+ (Math.round(kpageTotal * 100) / 100).toLocaleString()  +' / Total : '+ (Math.round(ktotal * 100) / 100).toLocaleString() +''
                  );

              }
          }
  };
}


// var shallowMerge = extend(obj1, obj2);
// var deepMerge = extend(true, obj1, obj2);

    var extend = function () {
      // Variables
      var extended = {};
      var deep = false;
      var i = 0;
      // Check if a deep merge
      if (typeof (arguments[0]) === 'boolean]') {
        deep = arguments[0];
        i++;
      }
      // Merge the object into the extended object
      var merge = function (obj) {
        if (obj.hasOwnProperty(prop)) {
          if (deep && Object.prototype.toString.call(obj[prop]) === '[object Object]') {
            // If we're doing a deep merge and the property is an object
            extended[prop] = extend(true, extended[prop], obj[prop]);
          } else {
            // Otherwise, do a regular merge
            extended[prop] = obj[prop];
          }
        }
      };
      // Loop through each object and conduct a merge
      for (; i < arguments.length; i++) {
        merge(arguments[i]);
      }

      return extended;
}



imgeHeader = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHYAAAAnCAIAAABVFysUAAAAIGNIUk0AAHomAACAhAAA+gAAAIDoAAB1MAAA6mAAADqYAAAXcJy6UTwAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfjBg0RMxWlfKZtAAAAXnRFWHRSYXcgcHJvZmlsZSB0eXBlIDhiaW0ACjhiaW0KICAgICAgMjgKMzg0MjQ5NGQwM2VkMDAwMDAwMDAwMDEwMDEyYzAwMDAwMDAxMDAwMTAxMmMwMDAwMDAwMTAwMDEKP27dsQAAARZ6VFh0UmF3IHByb2ZpbGUgdHlwZSBleGlmAABoge3YSWrEMBAF0H2dIkeoQeNxZFuB3CDHT2noNIFehAzQi18gWa4yT+Dlp/7+9kovXsGkUIi5pJoSe4UrXMzaeFUZLRY7/Km7l9cktJJTnfNVQmsQtZ71Go096HPKZ+tNzjvA5xpb55tw9nkx7Zt/XfQXCCBAgAABAgQIECBAgAABAgTouaGUs6aYjiTZditW5nHecYbM+EK4cV1Rxpc+CR+hapX7gM1rZiXe10f9RzEHcam3oOQz5+AfxBxP/LMBAQIECBAgQIAAAQIECBAgQP8JmRqbWLHmu/iuZjq66ucRTDSLFtZ39J0PR6XMnNWf0dfhS/x9pyj0AbGqZUpp6az+AAACgnpUWHRSYXcgcHJvZmlsZSB0eXBlIHhtcAAAWIXVmE2SozAMhfc6xRzBSLYEx3Fis+uqWc7x5wnyQ9KEJmFqCkIVOEh+1idLXkB/vn7TL/y4bQLJWXprLWijoidNFjkoa1LTTqsUttqfTqeeDe87jf4mmaRYJMRiIQp8W+0otpYNE5NYjjVFxROCIpjEJr3UUEKWs7WSrVVM1eLLacPB/+tZq4lkgkNhRjxRe49E8mi4uffMUiHE8FDJiWOM+ijiNoLRhVqLuIJkTOxt+HE1eHHVHo7GvTTS+YVREMadcS/jAlzIxJALX9taLr6C26cxIAAkBcisnQV4doi9IqaLHTSsPSFznkDPYBqSmVVhUknqjlES5IEugpwVROVJaPAOTxHxkT+F8MAewKEVxnSo4/eQAUyUGfyApVnPV3Ti3h1xT5iYPMUOCkDkhKsMycb4R1R6Zv0UlZ5Zf0Ydq+kZ1tF8p+q9kjAWjAry0GIvwwUy3CpqFMrR4RI8gBpRR8MADozqQBxjuLizoA+8FiTDgkgQZ4QggpYo52GZiSBdFW+C7rKY1vnE01KRvZN4Wiqyd2qMlopsTY05qHsRVO/Zx/SHTsdZY833Tp9rdPqk0+canR47Het2UtWGGE9wlOkujtbpmzFmP/wo5ND55eYhdTOoawqCJurD2XeZ+O1gHY9cDq8uem167/pfQkswh0NbD3MAtE9h9og2ZdlHRBuEXm/MAdHWVtkh0D5rmZ2i/Yv+3wvanWUvEW0QerUxh0RbV2UHQfukZXaLtr3/94N2ZdlPRBuE5jfmoGhrquwwaO+3zI7Rtvb/ntBGlj1FtEFobmNWCs1+Gk5ax4/AZmzkn3XpL17/gB6LA+j8AAAS5UlEQVRo3tVae3QURdb/VXfPJJNMnoSEkBgeIk9hPfgABFbUL7IHTiIuHgVFXVR0fcKBKILopy5izoIPFFd3RRQVBQUNGkFYHiIJCoRXggEJRAghGPKAJDPJzHR33e+PmzTDzGQIKp7z1blnku6uunXr1q3fvbeqBBHh3EIgE6YGDcBpnN6MzVuwZR/2VaDiNE574QUQich4xKcjvT/6D8XQkRh5KS4FICEBKFCYlYTk/7diaznKTZi48CIgJGQykrOQFfyVQALiG3xzAicUKAQKz4pAf8FfuqIrN/wV8lxwoXOLSSb/U0EVM2lmBmWAcF6KoZixNPZr+prbGmRYrPbQnmE07Gxl6Ucd4GzRIBpERJKkv7T82EANcRTXcVbP0rNEpJNOf0g5R8WsGh/5XqQXEyiBBRIkVFIVUgQJQcJ6KUgopPAnS/psyv6ZfmZWkmQ5lSdSItdXSPl1ZCObQsowGhasYlbTF/QFCFytI6wup8tNMgNY/REqZnGLqfgauob1pZFm6TQ88TSopIKQQilraS1zm0yTQbCT/WzlRqAOqAMaOmp0zHYIDQlWMdvEJJokSGikdVBUQWIX7bKa/0EqZo18SV/GUMwFKTeAbGTj5lVUZZDRnbqz/YIAEyDgKeBm4BZgWhtu/FoV8/+N1NiZOrPuAlTJKy+AG8/E0/Q0/VFYoQBg55aHvHEY14QmFaoBI7zfCONMACzG4lSkuuH2wEOgc1g1AY1AI9D0W70IO8/N2FyDGhWq1YuA4EcJyT5NhWq1YoechzwTpv/7i1cU7mkP9kzERAIpUNrz+yyrBo1JhRrgkXluXsbL9+AefgzhspOAZCAZSPqtojPzVVglIPw74ohIgdIVXZORbD1aKhYQpSjdi70C4tcFORemYgHhgWcSJnngUaDwJIdULstqwGAyYRLIMgQbbAaMmZg5HdN16NZo/bkAQALQGegMkSis2eoIBVgcd+2Cax3WsWAsp4CwwTYHc4pRXIrSUpQWonACJljhI889gVZhVaCEF6kQ0Wv0mgVSwWQFDLEUm0mZU2nqHJrzED00nIZzE4UUdmiTaTIDHIdrLnJ1oS5nUZKx+BHgBuBG4L6OYjFTP+rnj8XsqfIp3wJrlkQlNY/yggExh3KsmjyiftSPw56LjcVaC1pex+sc3ocwcigS0gHHTMy8D/elI93/637sn4/5H+ADH3xZyHoX754f4OwQdkGCYu2xUzCF3a0QIgBSWJuqoqIN4jOQEczMHyU0aAaMCZhwM24mUDGK38bbndF5GqbFI/4lvPQZPjuGYzwiAXEAB3Zj99W4+qKD8mpaHeyO/e03ndJ30k6eEJNMnXQmK0n5gD4YS2NbqIXa0g02jUArZoN9B8psBU+j75t9SZJpmMHTLs1Wy5IytImF5M9L6kP60CSzkiqTKIlHMZ7Gs1R30B1WNf59ip6iix9XKF/jawFh4ZRV2DRiELMGa67CVT742Bla4MjmICHvwl35yI9EJFdoH5IAAAdBewh74f7RbQpTUZVT7lP+EO8xPUIRRceLHlv2mBDCK70W9FuceMF9h+9+wS9W0sy/0YhWoBSjuBa1TjgjELERG5vRTCAHHAEc8pBnwLjYcYWyC7sC4yoAgApVQs7CrIEY6IPPDntweKBA4QiEOZwn5ZeAAbgAD9ACNMMwjHGvjLv0jktzluWoUL1eL5kUqUauKlpl6mbRgaIlW5fYFTtMBLg7lpZRwppU7v0X/EKgwRichjQXXF54x2JsNKIFxBEcsdoyVhzEwV3YddHjimRKDhm3gxBFUSfohEmmhQnBC9bCDX/ykc8gIxAomN6HeFDgQVy19KrbFt6GUUAmuj/U3eJZUlmy5Lsl179wfUFZwbXPXEtEuqkHdEpEbnJ3pa7+3pjX/gSawNXKqGwmzVxAC1zkkiSrqMpJTn9huP6T9OTFxgqlCU0Iil3YInqiZypS2VRDTo9A6MDLBpt/LtAKxLuBVwAP0AnojLLmsi82faHEKEITzZ5mU5oL1iwory2fvXx2SnxKbHRsvas+KjLqeP1xTdHIbzuQ1/hWbK1ClX+UyZa4FmtrUCMhe6FXLnJnYIYDDgGxFEtdcGnQLKn+MKzQ2guEAUQhir15MALwy3KUb8TGgC1EXnepSM1EZqt+BVAL/G/rvgTZCECD3oB4QAAKGpsar3zqyn2b9+Vn5U/JnJK/K793Wu+fa36Oj44/VnfsksRL/GXgvj7H54wSlvwE0qA1oOEdvDMbs33wsWAq1AY0vIbXAqImjpQP4VARioZi6MWLK7QIRHjhtXJf/2GcxEkvvBGICNYy59yFKHwAD4TkOwiDxmAMgSAAAuKBvwEbASfQCAD2GLvxkwEBKWVMdMzC+xY+73j+3sx71+xZc22fa3ce3jnwkoFurzsxOjFgajVoLWhZgzWcIvt/ZYR9Ha8/gkdiEctyKlD+g/9Uo5qjOv/6PEOrsGoohl68HETphE7Bb3mGj+M4O8OQlg4gAhEaNP61iB8TkHBWaAHYgPHAv4BRUBIUJOBPN/5p+BXDpUsKiChH1HV9rtv0/KZJwyYN7T10QNqAAxUH+qf1r22o7dOljySpiLPpL4BCFFaiMjgXlZAq1GpUv4E3BAQjgAuuYBP257Yaq3Xo5yDb76vinuiJNmTwL/zmBbzABh7S5xLICrYCKER9AyAgH7SDsBO1K2uXPr60+2XdyU1X9b3KlKbb6/bq3sf+57FtZdumZU9btG7RyMtHqopqStO/RwTFEv7FhCkgXsWrtajlTZKlWBqA2gGWVIayndjZXvJlea2O6zSwyWya7Z+DBqcez9Az3MDKOwwyPOQxyFhGy4Lb8uMIGqGTnkIpgQn04xA3CmQi/f50InLr7mXfL6tqrNJJ90mfTrpbdxNRYXnh0GeHNhvNXtMbEK40U/MldIl/LBFyu5LTCje5e1LPsxuq7VSeTtOD4wrTNP1zH8MwQqZCUkpd1/mTaZqmafo3MU0TBVTgf5wRcq92Ik0spdJg7l/Sl8GbG+2qmLO7f0HJUfAE+rzWp73kjST5mn1WjhdQvqPvwujX2nR3krOFWlbQivYMyN+MelEvH/kkSWvLwtJUQ0NDTU2Nx+M5b3BmDcftdp85c8Z6rw3BkIEYWIISFWrw6mZ38Qk+WYVVf8afh2FYL/RKQAKvu03YZCFaB9YPIIBKUDlBQb1Zv0KskCRNaaqKqgjFcqokSHW0bvj6Ixj72I/xcUhk8wcThuB/4B+FKAyfEDFWHMbh7dg+AiM4rpBSKopSWFg4Z86cwsJCXdeTkpJuueWWefPmJSUl7d2796677iKixsZGRVFSUlIyMzNnzJiRkJCwffv2Z555pqCgoKWlJTk5edSoUbNmzQIRvUvvhtlpC28F7VVuFygeBkYB1wOTz7fTdoHnp8G23MGaPPBpNI2xgvHhwIEDUVFRAMaNG5eTkzNgwAAAmZmZRFRSUgJAVdXMzMzrrrvO6XQCmDhxYl1dXVxcHIDx48dPnTp17NixUVFRb7/9NkwyfeQbRIPCq5JP5zTSNNL4mC7gYLSjKp4H/A2YDDwLlVRVqha3DtJ5ISIMhcGKntTTS15J0qf7iGjGjBkApk+fzou9qakpLS0NQHFxcXV1taIogwYN4k+HDx+OiorKyMj46KOPAGRlZVkQ0djY2NTUpBDIBtt7eM8GW5h9HP/9eBMmU0chgguvVxtgByIAOyw+F0RhOuWtkjAUBivKUb4d2wUEK2Dfvn1CiFtvvVVK6fV6nU5nYWHh1q1b09PTW1paGHa5eUpKiqIoUsrk5GQAW7ZsmTVr1ubNm0+fPh0TE+N0OjWG4MEY/B7em4RJfMBxUbZFGIuPAYcBgbaDkd+ncGSZhKQ85DngCM4GCXQH7ihDWcjojV+uxMqRGMnt2F85nU5FUQCsXbtW0zRN03RdZ+VWVVU98cQTHo9ny5YtLpcrKysrMzPz0UcfXbRoUW5ubm5ubkpKyvjx459//vlzTqCX0lLrCDnMeuwIFg+n4ZJkGqUFnkA/DmQCNwEP/A6A64+ngsQ9dE8Yjz+H5rTncljCHtTDQx5TmpLk8OHDAezcudMwjOrqamsyPvzww9raWv/pSUhIuPvuu2tra9m0f/jhh7lz544ZM0bTNAA33nhj4D2KAioYSAMtZYXHvjAqvoauIaIxNAZtJ/+tVAbsBnYDP/0+yvXX0Tf0jUmml7y8O2iRj3wmmXtpb8hjf38O39K3uq4bZEy8Y6KiKEuWLOHwtqKi4qabblJVNT8/n1Xct2/fM2fOnD59urm5OeSMlpSUOBwOACFuA7VQywJa0JN6BiiOfd15KYIiNNL48k4hFXJzG9k62PxXkJ3sGmkZlMEnLyGP4zjmHUyDLQkDKJIiNdKm0TRd13XSly1bBuCyyy774Ycfqqur169fn5iYKISorKysrKzUNM1yd0Tk9XqllEuXLr3zzjvXrFlz6NCh4uLi3NxcAF26dGn3TpuLXCtoxQSawKnUhdIAGsB8PqaPrbtbrXBh0e9nxSA8Qo9Q+zu//P4leik8kxiKaaRG0zS9Pu+ECRMCIDs3N5eIDhw4ACA9PV22FQboJUuWBNRPTExcvXq1oFA3M3k/hYiEEM1oLkPZIRyqQEUd6viQJozn4XOQDJkxQ5nBfE7gRB7yjuBIsBcN3sNjG+SD0YCalmcL6awexIP90d//MD+4o0pUzsd8XrtChEhJJOSTeDKd0kmQgNi4YeOOHTuamppSU1NHjBgxePBgInK5XHl5eXFxcdnZ2a3MiQAIIU6ePLlp06Yff/xRStm7d+/Ro0enpaWFULF/+e27qO2N+f9j4ayvva8hp01K2W4Dl9eVszzH/4aKj3xnz42s00zp4V/rpU66R3oWb118qukUAEFCQlqtdOj+G3Je6bXecIVNhzatKFrBn87WoXNa+cjHwnDvTCHXliEN/uWvhjQMGPkl+S+vf5mPYv1H0SqMNACsLFr5fsH7pmm6PW7DMFr3gEAEMkzDp/tM8+yKzFme4/K6hBBSSsOv8JRoIWYDJCB0Uy+tKEXbVSAAwbk+gTRFA6C18SGQEKK+qb6+qT42Mpa3egUEb/j67/xyefj9h1+58xVnhNPqNyM+Iz4yXoOmiNYDLWZ+jo20/bUJW3i7CxAPgAbt1JlT5b+Ua9AkJN9U57MrrqaTDuB43XGf6VNVNVqNDuSpagELu7SiVDd1AEIRWpBK27ViIUSkPZL/N6VJREVHi7IXZP/11b/uOraLiHymT0As/m5x9j+zx782ft3+dQB0Qwew/8T+bkndIm2RlnM/Wnf0/sX353ySU9NUQ0S8BXzwl4Prt60vKCto7QIEoOpMVfmpctYpv9xQumH0vNF/f//vLXoLc6uor7jl1VsmLpo49aOp/CZYfu7ihdUvjFsw7rbXbys5UcJzDMCm2iJtkWw9XO3NjW9m52Y/+sGjLq+Lm8c6YtfvWX/7wtsXrl8IwGf4AHyy/ZOb5998+xu3f/T9R9YSARBpjwyJ7OdRMfwcC9vm3M/nvjjxxQWTFuSuzhVCaIpW3Vi9ZMOSlTNWpiam7j66G4BJJoDyU+Xfln4LgBcXgBZfy9FTR7vEd4nQIgCwLffq3GtQ30FXdrsSAOegAEqOl3xf9j3riG8Jzft83vy75/978r/tqt2QhhBi7hdzR18x+q173yo6VBTaa5FUFfX7I99/W/LtqumrvIb3p5M/oc0vWYuAqx2qPrR6x+qvZn2VHJf85sY3baoNgNvrHpAxYMXUFZ8Vfnay4aRNtbl97re+eWv5tOV90/ruOLyDRxegqAtTMRG5PK62BwBIjEncsH/Df/f/Ny46juWL0CI6x3feXLr5jPtMVESUf1tr5Nx9nCNu7JVjP9366e6K3UIItiZN1byG16a1s9hFq9ElxSatK1731d6vGO8AdO3U9edTPxeUFVh2FGwcRJQQneB0ODcf2OzTfQ772XsqXt3b7G22tBwTGSMg8vfkHz11NDk2madBN/XDJw9v+HFDdGR0lD3KJFNTtHhn/JaDW47XHQ+YV5fHFSZqUJ977rmQxqsIJdYZ2y+1HwDmeEP/G77a/VWdq+7F215kY3TYHOV15SfqTxyrOXZpl0uv6XkNx1sOu6Nb5249knq0XlYT0A192+FtQ/oMyboiS1M0obReRItyRA3oOkBTNK4mhHBEOHp07pHRKUOI1taj+o9at29djatmVL9REVoEEY3sPXLb4W1V9VXjhozrn9qfm54zBCEIFB0Rvb9qf31T/ZHqI9f2ufaylMss8bond++e1J0XU2xkbJ/0Psu3Lr+699X3//l+dhidnJ18pm/fsX05WTndOnWTJO2q/YoeV3y67dNeqb2yB2d3je/aKiEQ7YgekNY2iuD5pgs5lfIvjGIPv/+wt9lrqMasm2f1S+0nIMKg0h9W+Daiy+ua8s6UaCUadsy7fV5yTHLw/USEis0vtEL48n+/pJ4+P6pgSQAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxOS0wNi0xM1QxNzo1NDo1OS0wNTowMEOf2iMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMTktMDYtMTNUMTc6NTE6MjEtMDU6MDDtwe6lAAAAEnRFWHRleGlmOkFydGlzdABncmVrYXOCgSowAAAAKnRFWHRleGlmOkRhdGVUaW1lRGlnaXRpemVkADIwMTg6MTE6MjMgMjI6MDM6NTR5QlMCAAAAKXRFWHRleGlmOkRhdGVUaW1lT3JpZ2luYWwAMjAxODoxMToyMyAyMjowMzo1NGDZPcIAAAAUdEVYdGV4aWY6RXhpZk9mZnNldAAyMTMwtnZf2wAAABt0RVh0ZXhpZjpTdWJTZWNUaW1lRGlnaXRpemVkADM2henpSQAAABp0RVh0ZXhpZjpTdWJTZWNUaW1lT3JpZ2luYWwAMzYYfwNVAAAARXRFWHRleGlmOldpblhQLUF1dGhvcgAxMDMsIDAsIDExNCwgMCwgMTAxLCAwLCAxMDcsIDAsIDk3LCAwLCAxMTUsIDAsIDAsIDBpn9LsAAAAMHRFWHRpY2M6Y29weXJpZ2h0AENvcHlyaWdodCAyMDAwIEFkb2JlIFN5c3RlbXMsIEluYy6pb7Z0AAAAKXRFWHRpY2M6ZGVzY3JpcHRpb24AVS5TLiBXZWIgQ29hdGVkIChTV09QKSB2MqZOuwAAAAAqdEVYdGljYzptYW51ZmFjdHVyZXIAVS5TLiBXZWIgQ29hdGVkIChTV09QKSB2MlLnGswAAAAjdEVYdGljYzptb2RlbABVLlMuIFdlYiBDb2F0ZWQgKFNXT1ApIHYyqVRIDAAAADV6VFh0dW5rbm93bgAASIntxrEJACAMALBXekAHqQ79/7JOgi8IgQypzqh9MtbVzwEAAIDvDb9NlS4Xb8fUAAAAJnRFWHR4bXA6Q3JlYXRlRGF0ZQAyMDE4LTExLTIzVDIyOjAzOjU0LjM2NGlA6hEAAAAASUVORK5CYII=';
